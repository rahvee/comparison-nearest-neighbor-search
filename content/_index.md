# Comparison of Nearest Neighbor Search Algorithms

Nearest Neighbor Search (NNS), also known as the Nearest Neighbor problem (NN), is the problem of searching a data set to find the closest or most similar data point to a given query point. The generalization of this problem is the kNN problem, finding the \\(k\\) nearest neighbors, and a variation is the Approximate Nearest Neighbor (ANN) problem, in which a successful search may return any data point whose distance is at most \\(c\\) times the distance of the exact nearest neighbor. In some applications, the data may be dynamic, thus requiring efficient support for insertions and deletions, as well as search.

![NNS](img/fig3.png)

For more overview of NNS, I recommend the [wikipedia article](https://en.wikipedia.org/wiki/Nearest_neighbor_search), which this page is intended to augment.



## Table of Contents

> 1. [Linear Search](#linear-search)
> 1. [k-d Tree Search](#k-d-tree-search)
> 1. [Vector approximation files](#vector-approximation-files)
> 1. [Compression/clustering based search](#compression-clustering-based-search)
> 1. [Quadtree and Octree](#quadtree-and-octree)
> 1. [Orchard's Algorithm and Annulus Algorithm](#orchards-algorithm-and-annulus-algorithm)
> 1. [Principal Component Partitioning (PCP)](#principal-component-partitioning-pcp)
> 1. [R-Tree dynamic](#r-tree-dynamic)
> 1. [vp tree](#vp-tree)
> 1. [bk tree](#bk-tree)
> 1. [Locality sensitive hashing](#locality-sensitive-hashing)
> 1. [Additional Resources and Remaining Questions](#additional-resources-and-remaining-questions)


## Linear Search

Unless you're familiar with [the dimensional curse problem](https://en.wikipedia.org/wiki/Curse_of_dimensionality), you may find it surprising that in a [1998 quantitative study](#weber-schek-blott), simple linear search was found to outperform space partitioning for high dimensionality (>10 dimensions). Weber et al. show that various partitioning and clustering techniques approach linear time at high dimensionality, and the low overhead of a simple linear search proves beneficial. They proposed an approximation technique called *vector approximation file* (or *VA-file*), that outperformed the other techniques in their study, when dimensionality is greater than ~6, and found that their technique actually *improves* in performance as dimensionality increases.

Although their findings remain relevant today, their study was conducted over 20 years ago, and the tipping point number of dimensions (to decide if you should use linear or partitioning search) is influenced by factors that have changed over time, such as software implementation, hardware capabilities, and number of data points. In the paper, they estimate their linear search algorithm benefited between 5x and 10x performance enhancement due to the performance characteristics of hard drives, which favor linear sequential access over random access. Modern hardware such as flash memory (SSD's, etc) have different performance characteristics, so the threshold number of dimensions may be different from the 10 or 6 described in the paper. Also, the data sets discussed in the paper were on the order of \\( 10^6 \\) data points, which might have seemed like a lot at the time, but would be considered a small data set today. A modern repeat of their experiments would be valuable, to discover how many dimensions should be considered "high dimensionality," and therefore favor linear search over partitioning. I found two sources ([Goodman & O'Rourke](#goodman-orourke) Chapter 39, and [Russell & Norvig](#russell-norvig) 740) that both suggested, without detailed justification, using partition search when \\(n \gg 2^d\\) and using linear search otherwise. (\\(n\\) being the number of data points within the data set, and \\(d\\) being the number of dimensions).

A simple intuitive summary of the underlying phenomenon in Weber et al.'s paper is as follows:

When using partitioning algorithms to perform NNS, as described in [k-d Tree Search](#k-d-tree-search), while the distance between the query point and the closest known data point is larger than the distance to a partition boundary, the boundary and the region on the other side of the boundary must be searched. The worst case occurs when the query point is very close to the convergence of many boundaries, and thus, searching a k-d tree is \\( O(n) \\) worst case.

![NNS worst case](img/fig2.png)

With increased dimensionality, the distance between any two points can only increase, and in general, *does increase*. (Unless the two points have exactly the same value in the new dimension). Therefore, the number of boundaries crossed by the \\( n \\)-sphere around the query point tends to increase, and high dimensionality results in \\( O(n) \\) performance *expected*. In these situations, an optimized linear search of \\( O(n)\\) is preferred over the poorly optimized partitioning algorithms that have degenerated into \\( O(n)\\) due to high dimensionality.

Another way to think about this dilemma is depicted in [What’s Wrong with High-Dimensional Similarity Search?](https://www.researchgate.net/profile/Stephen_Blott/publication/220538823_What's_wrong_with_high-dimensional_similarity_search/links/553e01f10cf2fbfe509b81f6.pdf) slides 51 to 54. With increased dimensionality, all the data points tend to approach equidistance from each other.

## k-d Tree Search

k-d Tree Search is a multi-dimensional search algorithm to find a data point in a \\(k\\)-dimensional set of data, inspired by the [binary search algorithm](https://en.wikipedia.org/wiki/Binary_search_algorithm), which easily achieves \\(O(\log n)\\) search time for sorted data, and \\( O(n\log n)\\) for unsorted data (achieving the computation-theoretical performance limit for comparison search), by recursively dividing the search space in two and eliminating half of the space on each iteration. Similarly, a k-d tree is constructed by finding the median data point in the first dimension, using that as the root of the tree, and then recursively dividing the two subtrees on the 2<sup>nd</sup> dimension, and dividing each of the four sub-subtrees on the 3<sup>rd</sup> dimension, and so on, until there is only one element on each side of the branch. In 1D, k-d tree search is equivalent to binary search, because it recursively divides the space in half along that dimension.

In an example 2D data set, we divide the space on the data point having the median \\(x\\) value:

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2.1.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2.1.png" alt="kdtree" /></td></tr></table>

And then divide the left side by its median \\(y\\) point:

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2.2.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2.2.png" alt="kdtree" /></td></tr></table>

And the right side by its median \\(y\\) point:

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2.3.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2.3.png" alt="kdtree" /></td></tr></table>

Each of those is again split by its median \\(x\\) point:

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2.4.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2.4.png" alt="kdtree" /></td></tr></table>

And so on, until there is at most one data point in each leaf of the tree:

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2.5.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2.5.png" alt="kdtree" /></td></tr></table>

At the root of the tree, finding the median takes \\(O(n)\\) time. One level down in the tree, 2 medians must be found, each requiring \\(O(\frac{n}{2})\\), so the total time for that level is also \\(O(n)\\). Two levels down, 4 medians must be found, each requiring \\(O(\frac{n}{4})\\), so the total time for that level is again \\(O(n)\\), and so on. In general, at each level \\(i\\) in the tree, with the root level being \\(i=0\\), there are \\(2^i\\) medians to calculate, each having size \\(\frac{n}{2^i}\\), so the total time at each level is \\(O(n)\\). Since the depth of the tree is \\(O(\log n)\\), and each level requires \\(O(n)\\) time, the total time complexity to build the whole tree is \\(O(n \log n)\\). The data structure must keep track of all \\(n\\) data points, with a relatively constant number of bytes per data point, so the data structure requires \\( O(n) \\) space.

Searching for a query point within the tree is a simple tree walk. If the query point is equal to the root of the tree, you're done. Otherwise, since the root of the tree split on the \\( x \\) value, if the query \\( x \\) value is less than the root \\( x \\) value, go left. Otherwise, go right. Repeat this process, following the splits on the \\( x \\) and \\( y \\) axes, until the query value is found, or discovered not to exist in the tree. The cost of the search is the depth into the tree where the answer is found or proven to be absent, so the search time is \\( O(\log n) \\).

Performing NNS is more complicated. Suppose there is a query point:

![search1](img/fig2-nns-1.png)

It's not equal to the root. The root splits on \\( x \\) and the query \\( x \\) value is greater than the root's \\( x \\) value, so move right in the tree.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-2.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-2.png" alt="kdtree" /></td></tr></table>

This node splits on \\( y \\). The query point is greater, so go right.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-3.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-3.png" alt="kdtree" /></td></tr></table>

This node splits on \\( x \\). The query point is less, so go left.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-4.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-4.png" alt="kdtree" /></td></tr></table>

The final split is on \\( y \\). The query point is greater, so go right. A leaf node has been found, so consider it the *closest-so-far*.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-5.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-5.png" alt="kdtree" /></td></tr></table>

The split boundary is closer than the *closest-so-far*, so consider the data point on the split boundary, and on the other side of the split boundary. Neither is closer, so keep the current *closest-so-far*.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-6.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-6.png" alt="kdtree" /></td></tr></table>

Keep walking back up the tree, and checking to see if the dividing hyperplane is closer than the *closest-so-far*. As long as the dividing plane is closer, the other side of the tree must be searched.

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-7.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-7.png" alt="kdtree" /></td></tr></table>

<table class="rahvee-table"><tr class="rahvee-table"><td class="rahvee-table"><img src="img/fig2-nns-8.png" alt="kdtree" /></td><td class="rahvee-table">\( \Longrightarrow \)</td><td class="rahvee-table"><img src="img/tree2-nns-8.png" alt="kdtree" /></td></tr></table>

![kdtree](img/tree2-nns-9.png)

The total expected time complexity for NNS using kd-tree, assuming randomized inputs, is \\( O(\log n)\\), but the analysis is tricky. For proof, see the original [1977 paper](#friedman-bentley-finkel) by Friedman, Bentley, and Finkel.

The worst case occurs when the query point is close to the convergence of many boundaries, and thus, searching a k-d tree is \\( O(n) \\) worst case. See [Linear Search](#linear-search). Whenever \\(n \gg 2^d\\), and the situation is randomized and not adversarial, the worst case is not *expected*. It only occurs very rarely (vanishingly), or in adversarial situations.

![NNS worst case](img/fig2.png)

## Vector approximation files

In 1997, [Weber and Blott](#weber-blott) argued that partitioning methods and clustering methods degenerate into linear search in high dimension, and they supported this claim with rigorous theoretical analysis, as well as quantitative experimental results. Their proposed method of "Vector Approximation Files," or "VA-files" work by partitioning the data into hyperrectangle cells (analogous to a grid of squares in 2D, or cubes in 3D) that are indexed by filter files. A search is conducted by starting in the cell with the query (the filter engaged with the narrowest possible focus), doing a linear scan of every data point in that cell, and progressively relaxing the filter, linearly scanning every data point in every cell, outward from the query cell, until the min bound of all remaining cells is greater than the "best so far" found in the search. The assumption is that high dimension searches will degenerate into full linear searches, so the #1 priority is to maintain optimal linear search behavior, but sometimes it doesn't need to *all* be scanned, so a lightweight heuristic that guides the linear search over sufficiently large sequential chunks can be helpful sometimes. A graphical explanation can be seen here, slides 29 to 33: [VLDB 2008 – What’s Wrong with High-Dimensional Similarity Search?](https://www.researchgate.net/profile/Stephen_Blott/publication/220538823_What's_wrong_with_high-dimensional_similarity_search/links/553e01f10cf2fbfe509b81f6.pdf).

VA-files have seen heavy usage over the years, and continuous research since their invention. Unfortunately, some public misconceptions have stuck through it all:

Despite their name, VA-files are used for *exact* nearest neighbor search, not approximate. Unfortunately, several resources describe VA-files as an ANN technique, which is not correct, and this misconception has proliferated. The nature of the confusion stems from the fact that approximation is used internally as a filter heuristic to guide the linear search. In 1997, nothing else did that, so Weber & Blott felt it was a fitting name. Over the years, however, a wealth of research and development went into ANN techniques, and VA-files got swallowed up in all the literature that uses the word "approximation." The term "approximation" grew to have a meaning different from the way Weber & Blott originally used it.

Along similar lines, since 1997 there has been a wealth of research and development in Vector Quantization (VQ). In 2007, [Ramaswamy and Rose](#ramaswamy-rose) pointed out that partitioning data into hyperrectangle cells is in fact a form of quantization lossy compression, a field that has advanced rapidly since VA-files were first invented, and newer, more powerful methods have since emerged in VQ. They point out that the name "Vector Approximation" files once again causes confusion, because Weber & Blott's quantization technique is actually quantizing each *component* of the feature vector independently and uniformly, so it would be more appropriately called *scalar* quantization. To quote R&R: "vector quantization, unlike the scalar quantization of the VA-File, can exploit dependencies across dimensions." They boast a 100x reduction of random IOPS compared to VA Files, and 3,000x reduction compared to another VQ technique. To make this result even more stunning, R&R's VQ technique is not an approximation technique - it's an *exact* NNS technique.

## <a name="compression-clustering-based-search"></a>Compression/clustering based search

Consider some real-value data points distributed on a number line:

![number line with precision](img/fig4a.png)

And consider what happens if the data is rounded, truncated, or binned:

![number line without precision](img/fig4b.png)

This is the process of *quantization*, a destructive and irreversible process that can be considered a form of lossy compression, and is the root cause of lossiness for many lossy algorithms. The data still resembles the original, but precision has been lost. By reducing the *cardinality*, i.e. reducing the range of data from a continuous range of real or continuous values to a finite set of discrete values, it is often possible to have very large reductions of time or space, to store or compute some value with acceptable precision.

In 2D, quantization reduces 2-dimensional continuous areas to a single point that represents that area. As with 1D data, there are multiple quantization techniques possible, including rounding, truncating, binning, etc.

Figure vq-2D from [data-compression.com](https://web.archive.org/web/20120905142150/http://www.data-compression.com:80/vq.shtml) shows a 2D Voronoi quantization:

![voronoi diagram vector quantization](img/vq-2D.gif)

In any quantization, the point at the center of each area is called a **centroid** or **codevector**, and the set of all centroids is called a **codebook**.

To quickly generate a codebook, Monte-Carlo (random) sampling may be used. The distance between a data point and its centroid is the **quantization error**, and the overall quality of a codebook is often measured as the mean-squared error of all data points. Adaptively, or iteratively, the codebook can be improved until an arbitrary level of precision, or an arbitrary level of error has been reached.

Figure lbgvq from [data-compression.com](https://web.archive.org/web/20120813015558/http://www.data-compression.com/vqanim.shtml) shows an iterative process of constructing a Voronoi quantization, until the quantization error for some set of data is reduced to an acceptable threshold:

![voronoi vector quantization iterative](img/lbgvq.gif)

Just as hashing algorithms (in the figure below) can use recursive levels of sub-quantization to organize data and make it quickly searchable without loss of precision at the leaves, generally speaking, quantization and sub-quantization can be used to organize multi-dimensional continuous-valued data, without *necessarily* sacrificing precision at the leaves. That is to say: Multidimensional continuous data can be quantized very coarsely at the top level, and then within each cell, it can be quantized a bit more precisely, and so on, until there is at most some constant number \\(k\\) nodes at each leaf, without loss of precision.

![hash tree](img/fig5.png)

The core concept that made VA Files good was the rearrangement of data such that similar data are stored sequentially in files, and then a quantization technique is used as a heuristic to guide the order in which the files are searched. This concept is preserved in Ramaswamy & Rose's VQ technique - all the data that map to the same centroid are stored together, so they can all be retrieved in a single operation. Also just like VA Files, R&R's VQ technique calculates lower bounds from a query to a cluster-dividing hyperplane (analogous to `minBnd` in VA Files), and calculates the maximum distance to any point within the adjacent cluster (analogous to `maxBnd`). The order of clusters searched is determined by ascending order distance to hyperplane boundaries, and the process is repeated until all remaining clusters are farther away than the \\(k^{th}\\) NN, and the exact \\(k\\) NN's are returned, guaranteed. Clearly, they took inspiration from VA Files.

Their big gains over VA Files are the result of more advanced VQ - using any clustering algorithm such as K-Means or Generalized Lloyd Algorithm (GLA) that are able to exploit vector quantization rather than just scalar quantization - and a more efficient storage format. While VA Files stores the compressed version of every vector, R&R's VQ technique only stores the centroids, which is sufficient to generate the hyperplanes when needed, and to find the actual data associated with the centroid, when needed.

## Quadtree and Octree

TODO - occasionally mentioned in literature, doesn't seem to be used for NNS all that much (suspecting the performance characteristics are on-par with k-d Tree, and not better). Seem to be mostly used for other purposes where they have a clear advantage.

There is a nice visualization of quadtree 2D map filtering (range selection) on [d3js.org](https://d3js.org/), here:

https://bl.ocks.org/mbostock/4343214

## <a name="orchards-algorithm-and-annulus-algorithm"></a>Orchard's Algorithm and Annulus Algorithm

TODO

## Principal Component Partitioning (PCP)

TODO

## R-Tree dynamic

TODO

## vp tree

TODO

Useful resource:

(See item 20) [https://www.eecs.tufts.edu/~aloupis/comp150/projects-spring17.html](https://www.eecs.tufts.edu/~aloupis/comp150/projects-spring17.html)

* [Vantage point trees](https://fribbels.github.io/vptree/writeup)
* [jump to demo](https://fribbels.github.io/vptree/vptree.html?type=steps&strokeWidth=2&size=600&n=1000&dotSize=1.5) (click in the space, and use arrows on keyboard)
* Bonus: [Euclidean shortest paths](https://fribbels.github.io/shortestpath/writeup.html)

## bk tree

TODO

## Locality sensitive hashing

TODO

Technique for ANN, provides guarantees on search quality, based on some assumptions.

## Additional Resources and Remaining Questions

* [d3-delaunay](https://github.com/d3/d3-delaunay) Self-described as "a fast, no-dependency library for computing the [Voronoi diagram](https://en.wikipedia.org/wiki/Voronoi_diagram) of a set of two-dimensional points." 
* Observablehq has some nice visualizations, although, it's not clear to me if they're open source or what. They require you to share your github email address and "sign up" before they let you see source (as far as I can tell). Of course you could view page source, but be observant of license terms.
  * [The Delaunay's Dual](https://beta.observablehq.com/@mbostock/the-delaunays-dual)
  * [Nearest Neighbor Graph](https://beta.observablehq.com/@mbostock/nearest-neighbor-graph)
  * [Voronoi Incircles](https://beta.observablehq.com/@mbostock/voronoi-incircles)
  * [D3 Voronoi Labels](https://beta.observablehq.com/@mbostock/d3-voronoi-labels)
  * [Delaunay-Voronoi](https://beta.observablehq.com/@mbostock/delaunay-voronoi/2)
* ANN TODO:
  * The section on [Compression/clustering based search](#compression-clustering-based-search) ended up being almost entirely about R&R's VQ technique for exact NNS. This was a surprise result, because quantization is expected to be used primarily in approximation techniques. Numerous ANN techniques exist using quantization. It would be good to explore ANN techniques that are based on quantization.
  * LSH resources:
      * Datar, Mayur, Nicole Immorlica, Piotr Indyk, and Vahab S. Mirrokni. "Locality-sensitive hashing scheme based on p-stable distributions." In *Proceedings of the twentieth annual symposium on Computational geometry*, pp. 253-262. ACM, 2004. [http://www.cs.princeton.edu/courses/archive/spring05/cos598E/bib/p253-datar.pdf](http://www.cs.princeton.edu/courses/archive/spring05/cos598E/bib/p253-datar.pdf)
      * Gionis, Aristides, Piotr Indyk, and Rajeev Motwani. "Similarity search in high dimensions via hashing." In *Vldb*, vol. 99, no. 6, pp. 518-529. 1999. [http://www.cs.princeton.edu/courses/archive/spring13/cos598C/Gionis.pdf](http://www.cs.princeton.edu/courses/archive/spring13/cos598C/Gionis.pdf)
      * Muja, Marius, and David G. Lowe. "Fast approximate nearest neighbors with automatic algorithm configuration." *VISAPP (1)*2, no. 331-340 (2009): 2. [https://lear.inrialpes.fr/~douze/enseignement/2014-2015/presentation_papers/muja_flann.pdf](https://lear.inrialpes.fr/~douze/enseignement/2014-2015/presentation_papers/muja_flann.pdf)

## References

1. <a name="weber-schek-blott"></a>Weber, Roger, Hans-Jörg Schek, and Stephen Blott. "A quantitative analysis and performance study for similarity-search methods in high-dimensional spaces." In *VLDB*, vol. 98, pp. 194-205. 1998. [http://www.vldb.org/conf/1998/p194.pdf](http://www.vldb.org/conf/1998/p194.pdf)
2. <a name="weber-blott"></a>Weber, Roger, and Stephen Blott. *An approximation based data structure for similarity search*. No. 9141. Technical Report 24, ESPRIT Project HERMES, 1997. [http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.40.480&rep=rep1&type=pdf](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.40.480&rep=rep1&type=pdf)
3. <a name="russell-norvig"></a>Russell, Staurt J., and Peter Norvig. "Artificial Intelligence (A Modern Approach)". (2010): 601, 738, 742, 814
4. Flach, Peter. "Machine learning: the art and science of algorithms that make sense of data". Cambridge University Press, 2012: 23, 242-49
5. Mitchell, Tom M. "Machine learning". McGraw-Hill. (1997): 231-33.
6. [Euclidean_Voronoi_diagram](https://en.wikipedia.org/wiki/Voronoi_diagram#/media/File:Euclidean_Voronoi_diagram.svg), by [Balu Ertl](https://commons.wikimedia.org/wiki/User:Balu.ertl) is licensed by [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
7. Brown, Russell A. "Building a Balanced k-d Tree in O(kn log n) Time." 2014, Journal of Computer Graphics Techniques (JCGT), Vol. 4, No. 1, 50-68, 2015. [https://arxiv.org/pdf/1410.5420.pdf](https://arxiv.org/pdf/1410.5420.pdf)
8. “Nearest Neighbor Search with Kd-Trees.” ALGLIB Project. Accessed July 30, 2018. http://www.alglib.net/other/nearestneighbors.php
9. Cormen, Thomas H., Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein. "Introduction to algorithms." MIT press, 2009. 215-19. 
10. <a name="friedman-bentley-finkel"></a>Friedman, Jerome, Jon Bentley, and Raphael Finkel. "An Algorithm for Finding Best Matches in Logarithmic Expected Time." ACM Transactions on Mathematical Software (TOMS) 3, no. 3 (1977): 209-26. [http://astrometry.net/svn/trunk/documents/papers/dstn-review/papers/friedman1977.pdf](http://astrometry.net/svn/trunk/documents/papers/dstn-review/papers/friedman1977.pdf)
11. <a name="goodman-orourke"></a>Goodman, Jacob E, and O'Rourke, Joseph. "Handbook of Discrete and Computational Geometry." 2nd ed. Discrete Mathematics and Its Applications. Boca Raton: Chapman & Hall/CRC, 2004.
12. JéGou, H., M. Douze, and C. Schmid. "Product Quantization for Nearest Neighbor Search." Pattern Analysis and Machine Intelligence, IEEE Transactions on 33, no. 1 (2011): 117-28.
13. Ramaswamy, Sharadh, and Kenneth Rose. "Adaptive Cluster Distance Bounding for High-Dimensional Indexing." Ieee Transactions On Knowledge And Data Engineering 23, no. 6 (2011): 815-30.
14. <a name="ramaswamy-rose"></a>Ramaswamy, Sharadh, and Kenneth Rose. "Adaptive Cluster-Distance Bounding for Nearest Neighbor Search in Image Databases." Image Processing, 2007. ICIP 2007. IEEE International Conference on 6 (2007): VI - 381-VI.
